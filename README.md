# **Déploiement de l'application**

Ce repo contient les configurations Docker compose nécesaire au déploiement de l'applicaiton
Il est à adapter en fonction de l'environnement de déploiement.

# Environnement de développement

Prérequis:

- Docker
- Docker Compose

Une fois les prérequis installés, cloner tous les répos connexes au projet, puis se placer dans le repo `deployement`:

    git clone https://gitlab.com/rmrfcesi/deployement.git
    git clone https://gitlab.com/rmrfcesi/back.git
    git clone https://gitlab.com/rmrfcesi/front.git
    cd deployement/

Déployer le stack:

    docker-compose up -d

# Environnement de production

Les prérequis sont les mêmes que pour l'environnement de développement, cependant, il est nécessaire d'utiliser le fichier `docker-compose.prod.yml`

    docker-compose -f docker-compose.prod.yml up -d

> Note: Pour effectuer un déploiement automatique Gitlab, il et nécessaire de génerer une paire de clés SSH et de copier la clé privée dans la variable CI $SSH_PRIVATE_KEY du projet

```bash
mkdir -m 644 -p ~/.ssh
touch ~/.ssh/authorized_keys
ssh-keygen -t ed25519 -f ~/.ssh/id_ed25519
cat ~/.ssh/id_ed25519.pub >> ~/.ssh/authorized_keys
```
